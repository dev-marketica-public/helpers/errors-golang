# Errors-golang - обработка ошибок и уведомление о них 

## Описание

При получении ошибоки обрабатываем и  отправляем текст ошибки и путь до ошибки, строка ошибки, название сервиса в telegram или на webhook.

## В будущем

- Тестирование
- Улучшение информации при получении ошибок

## Установка пакета


```bash
go get -u gitlab.com/dev-marketica-public/helpers/errors-golang
```


## Использование

Пример telegram:

[embedmd]:# (examples/bot/main.go)
```go
package main

import (
	"errors"
	"gitlab.com/dev-marketica-public/helpers/errors-golang"
	"log"
)

func main() {
	helper := helpererr.CreateHelper("1111", "", "-100111111", "test")
	err := helper.SendErrorToTelegram(errors.New("test")) // variable type error
	if err != nil {
		log.Fatal(err)
	}
}

```
Уведомление: 
```go
❗️ Error detected
Name service: test
Text error: test
Path error: /var/www/example/main.go
Line: 45
Time error (MSK) : 2022-03-17 20:56:26
```

## Примеры

Больше примеров в [example](https://gitlab.com/dev-marketica-public/helpers/errors-golang/-/tree/master/example) папке.

## Баги, фиксы, рефакторинг

Присылайте merge request с описанием.