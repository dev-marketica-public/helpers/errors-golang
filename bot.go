package helpererr

import (
	"errors"
	"net/http"
	"net/url"
)

type bot struct {
	Token       string
	ChannelID   string
	NameService string
}

// NewBot Init Bot instance
func newBot(token string, channelID string, nameService string) *bot {

	if len(token) == 0 {
		panic("Add telegram token")
	}

	return &bot{
		Token:       token,
		ChannelID:   channelID,
		NameService: nameService,
	}
}

// Send error to channel
func (bot *bot) send(textError string) error {
	if len(bot.ChannelID) == 0 {
		return errors.New("channel id is empty")
	}

	path, line := getPathLineError()

	processedTextErr := "❗️ <b>Error detected</b> \n"

	nameService := getNameService(bot.NameService)

	processedTextErr += "<b>Name service:</b> " + nameService + " \n"

	processedTextErr += "<b>Text error:</b> " + textError + " \n"

	processedTextErr += "<b>Path error: </b>" + path + " \n"

	processedTextErr += "<b>Line: </b>" + line + " \n"

	t, err := getTimeError()

	if err != nil {
		return err
	}

	processedTextErr += "<b>Time error (MSK) </b>: " + t

	err = sendMessageToChannel(bot.ChannelID, processedTextErr, bot.Token)

	return err
}

func sendMessageToChannel(chatID string, textErr string, token string) error {
	req := url.Values{}
	req.Set("chat_id", chatID)
	req.Set("text", textErr)
	req.Set("parse_mode", "HTML")
	resp, err := http.PostForm("https://api.telegram.org/bot"+token+"/sendMessage",
		req)

	if resp.StatusCode != 200 {
		return errors.New("request send message to telegram response code: " + resp.Status)
	}

	return err
}
