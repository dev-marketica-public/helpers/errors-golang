package helpererr

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type webhook struct {
	WebhookUrl  string
	NameService string
}

// NewWebhookSender Init Webhook instance
func newWebhookSender(webhookUrl string, nameService string) *webhook {
	if len(webhookUrl) == 0 {
		panic("Add webhook url")
	}

	return &webhook{
		WebhookUrl:  webhookUrl,
		NameService: nameService,
	}
}

// Send error to webhook
func (w *webhook) send(textError string) error {
	path, line := getPathLineError()

	nameService := getNameService(w.NameService)

	t, err := getTimeError()

	if err != nil {
		return err
	}

	errMap := map[string]string{
		"error":        textError,
		"line":         line,
		"path_error":   path,
		"time_error":   t,
		"name_service": nameService,
	}

	jsonErr, err := json.Marshal(errMap)

	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", w.WebhookUrl, bytes.NewBuffer(jsonErr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	_, err = client.Do(req)

	if err != nil {
		return err
	}

	return err
}
