package helpererr

import (
	"runtime"
	"strconv"
	"time"
)

type Helper struct {
	TelegramToken string
	WebhookUrl    string
	ChannelID     string
	NameService   string
}

type SendErrorToTelegram interface {
	SendErrorToTelegram(newErr error) error
}

type SendMessageInterface interface {
	send(textError string) error
}

type SendErrorToWebhook interface {
	SendErrorToWebhook(newErr error) error
}

type SendError interface {
	SendError(newErr error) error
}

// CreateHelper Initialize instance Helper
func CreateHelper(TelegramToken string, WebhookUrl string, ChannelID string, NameService string) *Helper {
	return &Helper{
		TelegramToken: TelegramToken,
		WebhookUrl:    WebhookUrl,
		ChannelID:     ChannelID,
		NameService:   NameService,
	}
}

// SendErrorToTelegram Send error to telegram channel
func (h *Helper) SendErrorToTelegram(newErr error) error {
	clientBot := newBot(h.TelegramToken, h.ChannelID, h.NameService)
	err := clientBot.send(newErr.Error())
	return err
}

// SendErrorToWebhook Send error to webhook
func (h *Helper) SendErrorToWebhook(newErr error) error {
	clientWebhook := newWebhookSender(h.WebhookUrl, h.NameService)
	err := clientWebhook.send(newErr.Error())
	return err
}

func (h *Helper) SendError(newErr error) error {

	var err error

	if len(h.TelegramToken) != 0 {
		err = h.SendErrorToTelegram(newErr)
		if err != nil {
			return err
		}
	}

	if len(h.WebhookUrl) != 0 {
		err = h.SendErrorToWebhook(newErr)
		if err != nil {
			return err
		}
	}

	return err
}

func getTimeError() (string, error) {
	location, err := time.LoadLocation("Europe/Moscow")

	if err != nil {
		return "", err
	}

	return time.Now().In(location).Format("2006-01-02 15:04:05"), nil
}

func getNameService(nameService string) string {
	if len(nameService) == 0 {
		nameService = "not defined"
	}
	return nameService
}

func getPathLineError() (string, string) {
	_, path, line, _ := runtime.Caller(3)
	return path, strconv.Itoa(line)
}
