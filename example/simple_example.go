package main

import (
	"errors"
	"gitlab.com/dev-marketica-public/helpers/errors-golang"
	"log"
)

var helper *helpererr.Helper

func init() {
	helper = helpererr.CreateHelper("111111", "https://google.com", "-1000000000", "test")
}

func main() {
	errorTelegram()
	errorWebhook()
	errorToWebhookTelegram()
}

func errorTelegram() {
	err := helper.SendErrorToTelegram(errors.New("test"))
	if err != nil {
		log.Fatal(err)
	}
}

func errorWebhook() {
	err := helper.SendErrorToWebhook(errors.New("test"))
	if err != nil {
		log.Fatal(err)
	}
}

func errorToWebhookTelegram() {
	err := helper.SendError(errors.New("test"))
	if err != nil {
		log.Fatal(err)
	}
}
