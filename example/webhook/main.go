package main

import (
	"errors"
	"gitlab.com/dev-marketica-public/helpers/errors-golang"
	"log"
)

func main() {
	helper := helpererr.CreateHelper("", "https://google.com", "", "test")
	err := helper.SendErrorToWebhook(errors.New("123"))
	if err != nil {
		log.Fatal(err)
	}
}
