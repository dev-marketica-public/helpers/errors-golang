package main

import (
	"errors"
	"gitlab.com/dev-marketica-public/helpers/errors-golang"
	"log"
)

func main() {
	helper := helpererr.CreateHelper("1111", "", "-100111111", "test")
	err := helper.SendErrorToTelegram(errors.New("test"))
	if err != nil {
		log.Fatal(err)
	}
}
