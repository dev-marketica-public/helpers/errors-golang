package main

import (
	"errors"
	"gitlab.com/dev-marketica-public/helpers/errors-golang"
	"log"
)

func main() {
	helper := helpererr.CreateHelper("111111", "https://google.com", "-100111111", "test")
	err := helper.SendError(errors.New("test"))
	if err != nil {
		log.Println(err)
	}
}
